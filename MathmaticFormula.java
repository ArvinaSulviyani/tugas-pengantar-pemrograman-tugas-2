import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MathmaticFormula {
   
	public static void showMenu() {
        
System.out.println("\nMenu Rumus");
System.out.println("1.  Persegi");
System.out.println("2.  Persegi Panjang");
System.out.println("3.  Segitiga Siku - Siku");
System.out.println("4.  Jajargenjang");
System.out.println("5.  Trapesium");
System.out.println("6.  Layang - Layang");
System.out.println("7.  Belah Ketupat");
System.out.println("8.  Lingkaran");
System.out.println("9.  Kubus");
System.out.println("10. Balok");
System.out.println("11. Limas");
System.out.println("12. Prisma");
System.out.println("13. Tabung");
System.out.println("14. Bola");
System.out.println("15. Kerucut");
System.out.println("0.  Keluar");
}
 
 	public static void squareFormula() {       
System.out.print("Masukan Panjang Sisi : ");
BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
      
  String inputData = null;
      
  try {
   inputData = bufferedReader.readLine();
        }
     catch (IOException error) {
        System.out.println("Error Input " + error.getMessage());
 }

 try  {
        float side = Float.parseFloat(inputData);
        float wide = side * side;
        System.out.println("Luas Persegi: " + wide);
        
        float perimeter = 4 * side;
        System.out.println("Keliling Persegi : " + perimeter);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
    }

    public static void rectangleFormula() {
        System.out.print("Masukan Panjang Sisi : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.print("Masukan Lebar Sisi : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
            inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        try  {
            float length = Float.parseFloat(inputDataLength);
            float width = Float.parseFloat(inputDataWidth);
            float wide = length * width;
            System.out.println("Luas Persegi Panjang: " + wide);
            
            float perimeter = ((2 * length) + (2 * width));
            System.out.println("Keliling Persegi Panjang : " + perimeter);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
    }
    
    public static void triangleFormula() {
        System.out.print("Masukan Tinggi Segitiga : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        try {
            inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.print("Masukan Sisi Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float high = Float.parseFloat(inputDataHigh);
            float side = Float.parseFloat(inputDataSide);
            double wide = 0.5 * high * side;
            System.out.println("Luas Segitiga : " + wide);
            
            double hypotenuse = (Math.sqrt(((high * high) + (side * side))));
            double perimeter = high + side + hypotenuse;
            System.out.println("Keliling Segitiga Siku - Siku : " + perimeter);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
    }
    
    public static void parallelogramFormula(){
      	 System.out.print("Masukan Tinggi Jajargenjang : ");
           BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
           String inputDataHigh = null;
           try {
               inputDataHigh = bufferedReader.readLine();
           }
           catch (IOException error) {
               System.out.println("Error Input " + error.getMessage());
           }
           
           System.out.print("Masukan Panjang Sisi Jajargenjang : ");
           bufferedReader = new BufferedReader(new InputStreamReader(System.in));
           String inputDataLength = null;
           try {
               inputDataLength = bufferedReader.readLine();
           }
           catch (IOException error) {
               System.out.println("Error Input " + error.getMessage());
           }
           
           System.out.print("Masukan Panjang Sisi Miring Jajargenjang : ");
           bufferedReader = new BufferedReader(new InputStreamReader(System.in));
           String inputDataHypotenuse = null;
           try {
               inputDataHypotenuse = bufferedReader.readLine();
           }
           catch (IOException error) {
               System.out.println("Error Input " + error.getMessage());
           }
           
           try  {
               float length = Float.parseFloat(inputDataHigh);
               float high = Float.parseFloat(inputDataHigh);
               float hypotenuse = Float.parseFloat(inputDataHypotenuse);
               double wide = length * high;
               System.out.println("Luas Jajagenjang : " + wide);
               
               double perimeter = ((2 *length)  + (2 *hypotenuse));
               System.out.println("Keliling Jajargenjang : " + perimeter);
           }
           catch(NumberFormatException e) {
               System.out.println("Masukan Anda Tidak Sesuai");
           }
      	
      }

    public static void trapezoidalFormula(){
    	 System.out.print("Masukan Tinggi Trapesium : ");
         BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
         String inputDataHigh = null;
         try {
             inputDataHigh = bufferedReader.readLine();
         }
         catch (IOException error) {
             System.out.println("Error Input " + error.getMessage());
         }
         
         System.out.print("Masukan Lebar Sisi Trapesium : ");
         bufferedReader = new BufferedReader(new InputStreamReader(System.in));
         String inputDataWidth = null;
         try {
             inputDataWidth = bufferedReader.readLine();
         }
         catch (IOException error) {
             System.out.print("Error Input " + error.getMessage());
         }
         
         System.out.println("Masukan Panjang Sisi Trapesium : ");
         bufferedReader = new BufferedReader(new InputStreamReader(System.in));
         String inputDataLength = null;
         try {
             inputDataLength = bufferedReader.readLine();
         }
         catch (IOException error) {
             System.out.println("Error Input " + error.getMessage());
         }
         
         try  {
             float high = Float.parseFloat(inputDataHigh);
             float width = Float.parseFloat(inputDataWidth);
             float length = Float.parseFloat(inputDataLength);
             double wide = 0.5 * (length + width) * high;
             System.out.println("Luas Trapesium : " + wide);
             
             double hypotenuse = (Math.sqrt((((length - width) * (length - width)) + (high * high))));
             double perimeter = length + width + hypotenuse + high;
             System.out.println("Keliling Trapesium : " + perimeter);
         }
         catch(NumberFormatException e) {
             System.out.println("Masukan Anda Tidak Sesuai");
         }
    	
    }

    public static void KiteFormula(){
    	System.out.print("Masukan Panjang Diagonal 1 : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal1 = null;
        try {
            inputDataDiagonal1 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Diagonal 2 : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal2 = null;
        try {
            inputDataDiagonal2 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.print("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Sisi Layang - Layang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Lebar Sisi Layang - Layang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
            inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	float diagonal1 = Float.parseFloat(inputDataDiagonal1);
        	float diagonal2 = Float.parseFloat(inputDataDiagonal2);
        	float length = Float.parseFloat(inputDataLength);
        	float width = Float.parseFloat(inputDataWidth);
        	double wide = 0.5 * diagonal1 * diagonal2;
        	System.out.println("Luas Layang - Layang : " + wide);
        	
        	double perimeter = (2 * (length + width));
        	System.out.println("Keliling Layang - Layang : " + perimeter);
        }
        
        catch (NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
    }
    
    public static void RhombusFormula(){
    	System.out.print("Masukan Diagonal Sisi 1 : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal1 = null;
        try {
            inputDataDiagonal1 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
    	System.out.print("Masukan Diagonal Sisi 2 : ");
    	bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal2 = null;
        try {
            inputDataDiagonal2 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Sisi Belah Ketupat : ");
    	bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	float diagonal1 = Float.parseFloat(inputDataDiagonal1);
        	float diagonal2 = Float.parseFloat(inputDataDiagonal2);
        	float side = Float.parseFloat(inputDataSide);
        	double wide = 0.5 * diagonal1 * diagonal2;
        	System.out.println("Luas Belah Ketupat : " + wide);
        	
        	double perimeter = 4 * side;
        	System.out.println("Keliling Belah Ketupat : " + perimeter);
        }
        catch(NumberFormatException e){
        	System.out.print("Masukan tidak sesuai");
        }
    }
    
    public static void CircleFormula(){
    	System.out.print("Masukan Jari - Jari Lingkaran : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRadius = null;
        try {
            inputDataRadius = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            float radius = Float.parseFloat(inputDataRadius);
            double wide = (3.14 * (radius * radius));
            System.out.println("Luas Lingkran: " + wide);
            
            double circumference = 2 * 3.14 * radius;
            System.out.println("Keliling Lingkaran : " + circumference);
            }
            catch(NumberFormatException e) {
                System.out.println("Masukan Anda Tidak Sesuai");
            }
    }
    
    public static void CubeFormula(){
    	System.out.print("Masukan panjang Sisi Kubus : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            float side = Float.parseFloat(inputDataSide);
            float volume = side * side * side;
            System.out.println("Volume Kubus : " + volume);
            
            float wide = (6 * (side * side));
            System.out.println("Luas Permukaan Kubus : " + wide);
            }
            catch(NumberFormatException e) {
                System.out.println("Masukan Anda Tidak Sesuai");
            }
    }
    
    public static void CuboidFormula(){
    	System.out.print("Masukan Panjang Balok : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Lebar Balok : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
            inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Tinggi Balok : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        try {
            inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	float length = Float.parseFloat(inputDataLength);
        	float width = Float.parseFloat(inputDataWidth);
        	float high = Float.parseFloat(inputDataHigh);
        	double volume = length * width * high;
        	System.out.println("Volume Balok : " + volume);
        	
        	double wide = (2 * length * width) + (2 * length * high) + (2 * width * high);
        	System.out.println("Luas Permukaan Balok : "+ wide);
        }
        	catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        	}
    }
    
    public static void PyramidFormula(){
    	System.out.print("Masukkan Panjang Sisi Limas Segiempat : ");
    	BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataLength = null;
    	try {
    		inputDataLength = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	System.out.print("Masukkan Tinggi Limas Segiempat : ");
    	bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataHigh = null;
    	try {
    		inputDataHigh = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	try {
    		float length = Float.parseFloat(inputDataLength);
    		float high = Float.parseFloat(inputDataHigh);
    		double volume = ((high * length * length)/3);
    		System.out.println("Volume Limas Segiempat : " + volume);
    		
    		double trianglehigh = ((high * high) + ((0.5 * length) * (0.5 * length)));
    		double wide = ((4 * (0.5 * length * trianglehigh)) + (length * length ));
    		System.out.println("Luas Permukaan Limas Segiempat : " + wide);
    	}
    		catch(NumberFormatException e){
    			System.out.println("Masukan tidak sesuai");
    		}
    	
    }

    public static void PrismFormula(){
    	System.out.print("Masukan Panjang Sisi Segitiga : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan Tinggi Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataTriangleHigh = null;
        try {
            inputDataTriangleHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Tinggi Prisma : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        try {
            inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	float side = Float.parseFloat(inputDataSide);
        	float high = Float.parseFloat(inputDataHigh);
        	float triangleHigh = Float.parseFloat(inputDataTriangleHigh);
        	double volume = 0.5 * side * triangleHigh * high;
        	System.out.println("Volume Prisma : " + volume);
        	
        	double hypotenuse = (Math.sqrt(((triangleHigh * triangleHigh) + (0.5 * (side * side)))));
            double perimeter = (side + (2 * hypotenuse));
        	double wide = ((2 * (0.5 * side * triangleHigh)) + (perimeter * high));
        	System.out.println("Luas Permukaan Prisma : " + wide);
        }
        catch(NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
    }
    
    public static void CylinderFormula(){
    	System.out.print("Masukkan Jari - Jari Lingkaran : ");
    	BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataRadius = null;
    	try {
    		inputDataRadius = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	System.out.print("Masukkan Tinggi Tabung : ");
    	bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataHigh = null;
    	try {
    		inputDataHigh = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	try {
    		float radius = Float.parseFloat(inputDataRadius);
    		float high = Float.parseFloat(inputDataHigh);
    		double volume = (3.14 * (radius * radius) * high);
    		System.out.println("Volume Tabung : " + volume);
    		
    		double wide = ((2 * 3.14 * radius * high) + (2 * 3.14 * (radius * radius)));
    		System.out.println("Luas Permukaan Tabung : " + wide);
    	}
    		catch(NumberFormatException e){
    			System.out.println("Masukan tidak sesuai");
    		}
    	
    }
    
    public static void SphereFormula(){
    	System.out.print("Masukan Jari - Jari Lingkaran : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRadius = null;
        try {
            inputDataRadius = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	float radius = Float.parseFloat(inputDataRadius);
        	double volume = (4/3 * 3.14 * radius * radius * radius);
        	System.out.println("Volume Bola : " + volume);
        	
        	double wide = 4 * 3.14 * radius * radius;
        	System.out.println("Luas Permukaan Bola : " + wide);
        }
        catch(NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
    }
    
    public static void ConeFormula(){
    	System.out.print("Masukan Jari - Jari Lingkaran : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRadius = null;
        try {
            inputDataRadius = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Tinggi Kerucut : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        try {
            inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        try {
        	float radius = Float.parseFloat(inputDataRadius);
        	float high = Float.parseFloat(inputDataHigh);
        	double volume = ((3.14 * radius * radius * high)/3);
        	System.out.println("Volume Kerucut : " + volume);
        	
        	double hypotenuse = (Math.sqrt(((high * high) + (radius * radius))));
        	double wide = ((3.14 * radius * hypotenuse) + (3.14 * radius * radius));
        	System.out.println("Luas Permukaan Kerucut : " + wide);
        }
        catch(NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
    }
   
    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;

        do {
            showMenu();
            System.out.print("Masukkan Pilihan Anda : ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        squareFormula();
                    }
                    else if (choice > 0 && choice == 2) {
                        rectangleFormula();
                    }
                    else if (choice > 0 && choice == 3) {
                        triangleFormula();
                    }
                    else if (choice > 0 && choice == 4) {
                        parallelogramFormula();
                    }
                    else if (choice > 0 && choice == 5) {
                    	trapezoidalFormula();
                    }
                    else if (choice > 0 && choice == 6) {
                    	KiteFormula();
                    }
                    else if (choice > 0 && choice == 7) {
                    	RhombusFormula();
                    }
                    else if (choice > 0 && choice == 8) {
                    	CircleFormula();
                    }
                    else if (choice > 0 && choice == 9) {
                    	CubeFormula();
                    }
                    else if (choice > 0 && choice == 10) {
                    	CuboidFormula();
                    }
                    else if (choice > 0 && choice == 11) {
                    	PyramidFormula();
                    }
                    else if (choice > 0 && choice == 12) {
                    	PrismFormula();
                    }
                    else if (choice > 0 && choice == 13) {
                    	CylinderFormula();
                    }
                    else if (choice > 0 && choice == 14) {
                    	SphereFormula();
                    }
                    else if (choice > 0 && choice == 15) {
                    	ConeFormula();
                    }
                    else {
                        System.out.println("TERIMA KASIH");
                    }
                }

                catch(NumberFormatException e) {

                    System.out.println("Masukan Anda Tidak Sesuai");

                }

            }

            catch (IOException error) {

                System.out.println("Error Input " + error.getMessage());

            }


        } while(choice > 0);

    }

}
