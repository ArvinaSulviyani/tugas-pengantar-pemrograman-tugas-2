
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class TicTacToe
{
	// Grid variables
	//    0 for an empty square
	//    1 if the square contains X
	//    2 if the square contains O
	static int A, B, C, D, E, F, G, H, I;

	
	static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args)
	{
		System.out.println("\n--WELCOME To TIC TAC TOE--");
		System.out.println();
		System.out.println("Inputan : a, b, c, d, e, f, g");
		Board();
		String prompt = "Masukkan Langkah Pertama: ";
		String humanMove = "";
		String computerMove = "";
		boolean gameIsWon = false;

		for (int index = 1; index <=9; index++)
		{
			humanMove = getMove(prompt);
			updateBoard(humanMove, 1);
			displayBoard();
			if (isGameWon())
			{
				System.out.println("Anda Menang!");
				gameIsWon = true;
				break;
			}

			if (index < 9)
			{
				computerMove = getComputerMove();
				System.out.println("Saya akan bermain di " + computerMove);
				updateBoard(computerMove, 2);
				displayBoard();
				if (isGameWon())
				{
					System.out.println("Anda Kalah!");
					gameIsWon = true;
					break;
				}
				prompt = "Masukkan Langkah Selanjutnya: ";
				index++;
			}
		}
		if (!gameIsWon)
			System.out.println("Permainan Seri!");
	}

	public static String getMove(String prompt)
	{
		
		String play = null;
		System.out.print(prompt);
		do
		{
			try {
				play = bufferedReader.readLine();
			}
			catch(IOException error){
				System.out.print("Error input " + error.getMessage());
			}
			try {
				int inputData = Integer.parseInt(prompt);
			}
			catch(NumberFormatException e){
				System.out.println("Masukan tidak sesuai");
			}
			
		} while (!ValidPlay(play));
		return play;
	}

	public static boolean ValidPlay(String play)
	{
		if (play.equalsIgnoreCase("A") & A == 0)
			return true;
		if (play.equalsIgnoreCase("B") & B == 0)
			return true;
		if (play.equalsIgnoreCase("C") & C == 0)
			return true;
		if (play.equalsIgnoreCase("D") & D == 0)
			return true;
		if (play.equalsIgnoreCase("E") & E == 0)
			return true;
		if (play.equalsIgnoreCase("F") & F == 0)
			return true;
		if (play.equalsIgnoreCase("G") & G == 0)
			return true;
		if (play.equalsIgnoreCase("H") & H == 0)
			return true;
		if (play.equalsIgnoreCase("I") & I == 0)
			return true;
		return false;
	}

	public static void updateBoard(String play, int player)
	{
		if (play.equalsIgnoreCase("A"))
			A = player;
		if (play.equalsIgnoreCase("B"))
			B = player;
		if (play.equalsIgnoreCase("C"))
			C = player;
		if (play.equalsIgnoreCase("D"))
			D = player;
		if (play.equalsIgnoreCase("E"))
			E = player;
		if (play.equalsIgnoreCase("F"))
			F = player;
		if (play.equalsIgnoreCase("G"))
			G = player;
		if (play.equalsIgnoreCase("H"))
			H = player;
		if (play.equalsIgnoreCase("I"))
			I = player;
	}
	
	public static void Board()
	{
		String line = "";
		System.out.println();
		line = " a | b | c" ;
		System.out.println(line);
		System.out.println("-----------");
		line = " d | e | f";
		System.out.println(line);
		System.out.println("-----------");
		line = " g | h | i";
		System.out.println(line);
		System.out.println();
	}
	
	public static void displayBoard()
	{
		String line = "";
		System.out.println();
		line = " " + getXO(A) + " | " + getXO(B) + " | " + getXO(C);
		System.out.println(line);
		System.out.println("-----------");
		line = " " + getXO(D) + " | " + getXO(E) + " | " + getXO(F);
		System.out.println(line);
		System.out.println("-----------");
		line = " " + getXO(G) + " | " + getXO(H) + " | " + getXO(I);
		System.out.println(line);
		System.out.println();
	}

	public static String getXO(int square)
	{
		if (square == 1)
			return "X";
		if (square == 2)
			return "O";
		return " ";
	}

	public static String getComputerMove()
	{
		if (A == 0)
			return "A";
		if (B == 0)
			return "B";
		if (C == 0)
			return  "C";
		if (D == 0)
			return  "D";
		if (E == 0)
			return  "E";
		if (F == 0)
			return  "F";
		if (G == 0)
			return  "G";
		if (H == 0)
			return  "H";
		if (I == 0)
			return  "I";
		return "";
	}

	public static boolean isGameWon()
	{
		if (isWon(A, B, C))
			return true;
		if (isWon(D, E, F))
			return true;
		if (isWon(G, H, I))
			return true;
		if (isWon(A, D, G))
			return true;
		if (isWon(B, E, H))
			return true;
		if (isWon(C, F, I))
			return true;
		if (isWon(A, E, I))
			return true;
		if (isWon(C, E, G))
			return true;
		return false;
	}

	public static boolean isWon(int Index1, int Index2, int Index3)
	{
		return ((Index1 == Index2) & (Index1 == Index3) & (Index1 != 0));
	}
}

